/**
 * Copyright (C) 2020 ExploRêve
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Full source code is available at <https://gitlab.com/exploreve/frontend>.
 */

var music2 = document.querySelector("#music2");
var music3 = document.querySelector("#music3");

function handlePlayError() {}

music2.play().catch(handlePlayError);

if (typeof L === "undefined") {
  var L = {};
}

var Thunderforest_Pioneer = L.tileLayer("https://cdn.exploreve.fr/tiles/{z}/{x}/{y}.png", {
  attribution: `Fond de carte : &copy; <a href="https://www.thunderforest.com/">Thunderforest</a>, Données : &copy; les contributeurs d'<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>`,
  maxZoom: 20,
  minZoom: 20
});

var map = L.map("mapid", {
  center: [46.92097, -0.84756],
  zoom: 20,
  zoomControl: false,
  layers: [Thunderforest_Pioneer]
});

var last_pos = map.getCenter();

var erwinIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/personnages/erwin.svg",
  iconSize: [80, 250], // size of the icon 363*1136
  iconAnchor: [40, 250], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -255] // point from which the popup should open relative to the iconAnchor
});

var dariaIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/personnages/daria.svg",
  iconSize: [210, 250], // size of the icon 822*975
  iconAnchor: [105, 250], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -255] // point from which the popup should open relative to the iconAnchor
});

var maisonIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/maison.svg",
  iconSize: [222, 250], // size of the icon 637*713
  iconAnchor: [111, 250], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -255] // point from which the popup should open relative to the iconAnchor
});

var fantomeIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/fantome.svg",
  iconSize: [334, 200], // size of the icon 1605*962
  iconAnchor: [167, 200], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -205] // point from which the popup should open relative to the iconAnchor
});

var codexIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/sac_a_dos/codex.svg",
  iconSize: [94, 150], // size of the icon 440*701
  iconAnchor: [47, 150], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -155] // point from which the popup should open relative to the iconAnchor
});

var boussoleIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/boussole.svg",
  iconSize: [158, 150], // size of the icon 382*362
  iconAnchor: [79, 150], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -155] // point from which the popup should open relative to the iconAnchor
});

var canotIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/canot.svg",
  iconSize: [350, 150], // size of the icon 1181*506
  iconAnchor: [175, 150], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -155] // point from which the popup should open relative to the iconAnchor
});

var pierreIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/pierre.svg",
  iconSize: [148, 250], // size of the icon 715*589
  iconAnchor: [74, 250], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -200] // point from which the popup should open relative to the iconAnchor
});

var papierIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/papier.svg",
  iconSize: [146, 150], // size of the icon 577*593
  iconAnchor: [73, 150], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -155] // point from which the popup should open relative to the iconAnchor
});

var pecheuseIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/personnages/pecheuse.svg",
  iconSize: [192, 250], // size of the icon 1015*1318
  iconAnchor: [96, 250], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -255] // point from which the popup should open relative to the iconAnchor
});

var panneauIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/panneau_templier.svg",
  iconSize: [190, 250], // size of the icon 615*808
  iconAnchor: [95, 250], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -255] // point from which the popup should open relative to the iconAnchor
});

var pancarteIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/panneau_lorem.svg",
  iconSize: [268, 250], // size of the icon 1179*1100
  iconAnchor: [134, 250], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -255] // point from which the popup should open relative to the iconAnchor
});

var afficheIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/labyrinthe.svg",
  iconSize: [180, 250], // size of the icon 554*771
  iconAnchor: [90, 250], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -255] // point from which the popup should open relative to the iconAnchor
});

var leonIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/personnages/leon.svg",
  iconSize: [114, 250], // size of the icon 626*1361
  iconAnchor: [57, 250], // point of the icon which will correspond to marker's location
  popupAnchor: [0, -255] // point from which the popup should open relative to the iconAnchor
});

var grosCaillouIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/cailloux.svg",
  iconSize: [136, 150], // 442*488
  iconAnchor: [68, 150],
  popupAnchor: [0, -155]
});

var petitCaillouIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/cailloux.svg",
  iconSize: [68, 75], // 442*488
  iconAnchor: [34, 75]
});

var oiseauIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/oiseau_songe.svg",
  iconSize: [326, 200], // 1453*893
  iconAnchor: [163, 200],
  popupAnchor: [0, -205]
});

var moulinIcon = L.icon({
  iconUrl: "https://cdn.exploreve.fr/content/carte/moulin.svg",
  iconSize: [258, 250], // 1102*1069
  iconAnchor: [179, 250]
});

var etape1 = L.marker([46.92087, -0.84756], { icon: codexIcon }).addTo(map);
etape1
  .bindPopup(
    `<h2 id="popuptitle">1. L'arrivée à Treize-Vent</h2>
  <p id="popup">L'endroit dans lequel tu as atterri ne paraît pas être le lieu idéal pour installer ton campement. En observant les alentours, tu trouves un livre dont le titre est Codex. Tu te dis que ça pourra peut-être servir, et tu le déposes dans ton sac.</br>
  </br>
  Tu aperçois au loin un groupe de jeunes avec des foulards, probablement arrivé.es juste avant toi... Le groupe semble se diriger vers la rue de la colonne... Tu décides de les rattraper ! Ils et elles continuent ensuite sur la rue des écoles.</p>`
  )
  .openPopup();

var etape2 = L.marker([46.92271, -0.85192], { icon: papierIcon }).addTo(map);
etape2
  .bindPopup(
    `<h2 id="popuptitle">2. Le mystérieux message</h2>
  <p id="popup">Mince, tu as perdu leur trace&nbsp;! Alors que tu regardes autour de toi, tu t'aperçois qu’une personne a fait tomber un morceau de papier. Tu le ramasses et lis la phrase qui y est inscrite&nbsp;:</p>
  <blockquote id="citation">
    Cqhsxuh luhi bu deht zkigk’q Bq Rektydyuhu
  </blockquote>
  <p id="popup">Décode ce message pour savoir où aller. Si tu es en difficulté, rappelle-toi que tu as mis quelque chose dans ton sac.</p>`
  )
  .on("click", marker_click);

var etape3 = L.marker([46.92563, -0.85455], { icon: maisonIcon }).addTo(map);
etape3
  .bindPopup(
    `<h2 id="popuptitle">3. La fenêtre scintillante</h2>
  <p id="popup">Le village de La Boudinière semble désert... à l'exception, peut-être, d'une lumière que tu vois scintiller à une fenêtre, près de l'endroit où tu te trouves. </br>
  Tu t'approches prudemment... </p>
  <video controls id="media">
    <source src="https://cdn.exploreve.fr/content/videos/morse.mp4" type="video/mp4">
    <source src="https://cdn.exploreve.fr/content/videos/morse.webm" type="video/webm">
    Vidéo non suportée
  </video>
  <p id="popup"><a href="https://cdn.exploreve.fr/content/transcriptions/morse.pdf" target="_blank" id="transcription">Transcription du message</a></p>`
  )
  .on("click", marker3_click);

var etape4 = L.marker([46.92707, -0.86487], { icon: boussoleIcon }).addTo(map);
etape4
  .bindPopup(
    `<h2 id="popuptitle">4. La boussole</h2>
  <p id="popup">Tiens, une boussole. Tu la retournes et sur l’arrière est gravé&nbsp;:</p>
  <blockquote id="citation">Azimut 260</blockquote>
  <img id="boussole" src="https://cdn.exploreve.fr/content/carte/boussole.jpg" alt="Boussole"></img>`
  )
  .on("click", marker4_click);

var etape5 = L.marker([46.92688, -0.87186], { icon: canotIcon }).addTo(map);
etape5
  .bindPopup(
    `<h2 id="popuptitle">5. La rivière</h2>
  <p id="popup">Après avoir traversé un taillis, tu aperçois une rivière. Un canot est échoué sur la berge. Pourquoi ne pas le prendre et descendre la rivière vers le sud ?</p>`
  )
  .on("click", marker5_click);

var etape6 = L.marker([46.92419, -0.87294], { icon: pierreIcon }).addTo(map);
etape6
  .bindPopup(
    `<h2 id="popuptitle">6. L'île</2>
  <p id="popup">Sur l'île, une pierre t'intrigue. Tu t'approches et constates qu'un poème y est gravé</p>
  <blockquote id="citation">
    Libre comme l'air l'oiseau s'élance</br>
    Accroche son regard vers plus loin, devant</br>
    Nourri d'espoirs et de belles mélodies</br>
    Digne héritier des découvreurs d’hier et des exploratrices d'aujourd’hui</br>
    Esquivant tempêtes et machines d’acier</br>
    Beau et majestueux, qui pourrait l’arrêter&nbsp;?</br>
    Admire son exemple, toi qui sur la terre trouves ton appui</br>
    Usurpe son trône, avance-toi à ton tour</br>
    Déploie tes ailes, laisse derrière toi le train-train de tous les jours</br>
    Illumine de tes songes fabuleux ce monde endormi</br>
    Et ose l'aventure, sors de ta solitude</br>
    Retrouve tes semblables, et pour chasser l’ennui</br>
    Explorêvez ensemble ce monde infini</br>
  </blockquote>
  <p id="popup">Tu retournes la pierre et remarques une autre gravure : </p>
  <blockquote id="citation">Acrostiche</blockquote>
  <p id="popup">Qu'est-ce que cela peut bien vouloir dire ? Soudain, Eurêka ! Tu as compris. </br>
  Maintenant que tu as compris quelle est ta nouvelle destination, tu peux continuer ta route en suivant la rivière vers le sud. Peut-être croiseras-tu une personne qui pourra t'indiquer où trouver cette destination mystérieuse !</p>`
  )
  .on("click", marker6_click);

var etape67 = L.marker([46.9159, -0.8736], { icon: oiseauIcon }).addTo(map);
etape67
  .bindPopup(
    `<h2 id="popuptitle">L'oiseau songe</h2>
  <audio controls src="https://cdn.exploreve.fr/content/voix/oiseau_chelou.mp3" id="media">Audio non supporté</audio>
  <p id="popup"><a href="https://cdn.exploreve.fr/content/transcriptions/oiseau_songe.pdf" target="_blank" id="transcription">Transcription pour personnes sourdes et malentendantes</a></p>`
  )
  .on("click", marker_click);

var etape7 = L.marker([46.91079, -0.86593], { icon: pecheuseIcon }).addTo(map);
etape7
  .bindPopup(
    `<h2 id="popuptitle">7. Le pont</2>
  <p id="popup">Tu continues ta route et, sur un pont, tu aperçois une pêcheuse. Peut-être qu'elle pourra t'aider à trouver le chemin à prendre ? </p>
  <audio controls src="https://cdn.exploreve.fr/content/voix/pecheuse.mp3" id="media">Audio non supporté</audio>
  <p id="popup"><a href="https://cdn.exploreve.fr/content/transcriptions/la_pecheuse.pdf" target="_blank" id="transcription">Transcription pour personnes sourdes et malentendantes</a></p>`
  )
  .on("click", marker7_click);

var etape8 = L.marker([46.91115, -0.86542], { icon: erwinIcon }).addTo(map);
etape8
  .bindPopup(
    `<h2 id="popuptitle">8. Erwin</h2>
  <p id="popup">Tu rencontres un garçon, en train d’écrire sur un parchemin. </br>
  Il te propose de lire son «&nbsp;Parchemin de l’Explorêve&nbsp;», et t’explique que, plus tard, tu pourras le relire quand tu voudras, même sans l’avoir avec toi. </br>
  C’est un phénomène bizarre, qu’il a décidé d’appeler la <i>Réminiscience</i>.</br>
  Tu trouveras ce parchemin dans ton sac.</p>`
  )
  .on("click", marker_click);

L.marker([46.90152, -0.8602], { icon: moulinIcon }).addTo(map);

var etape9 = L.marker([46.89926, -0.86339], { icon: grosCaillouIcon }).addTo(map);
etape9
  .bindPopup(
    `<h2 id="popuptitle">9. Landebaudière</h2>
  <p id="popup">Tu es arrivé·e à Landebaudière ! Mais... il ne semble pas y avoir grand chose d'intéressant. </br>
  Alors que tu observes les environs, tu aperçois, un peu plus loin, une trainée de cailloux... Cela t'intrigue : est-ce qu’on t'indiquerait discrètement la prochaine direction à prendre ? </br>
  Tu te remets en route !</p>`
  )
  .on("click", marker_click);

L.marker([46.89909, -0.864], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89892, -0.86461], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89875, -0.86522], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89858, -0.86583], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89841, -0.86644], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89824, -0.86705], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89808, -0.86766], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89791, -0.86827], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89774, -0.86888], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89757, -0.86948], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.8974, -0.87009], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89723, -0.8707], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89706, -0.87131], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89689, -0.87192], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89672, -0.87253], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89655, -0.87314], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89638, -0.87375], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89621, -0.87436], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89604, -0.87497], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89588, -0.87558], { icon: petitCaillouIcon }).addTo(map);
L.marker([46.89571, -0.87619], { icon: petitCaillouIcon }).addTo(map);

var etape10 = L.marker([46.89555, -0.87675], { icon: panneauIcon }).addTo(map);
etape10
  .bindPopup(
    `<h2 id="popuptitle">10. L'étrange pancarte</h2>
  <p id="popup">Alors que tu continues d'avancer, tu aperçois une pancarte et t'approches...</p>
  <img id="media" src="https://cdn.exploreve.fr/content/carte/pancarte_templiers.svg" alt="Code des Templiers"></img>`
  )
  .on("click", marker_click);

var etape11 = L.marker([46.904, -0.87287], { icon: fantomeIcon }).addTo(map);
etape11
  .bindPopup(
    `<h2 id="popuptitle">11. Les fantômes de l'ennui</h2>
  <p id="popup">Tu te retrouves face à un être lent et translucide entouré d'une brume violette... Il s'avance lentement vers toi en glissant sur le sol et en poussant un gémissement...</p>`
  )
  .on("click", marker_click);

var etape12 = L.marker([46.90415, -0.87267], { icon: dariaIcon }).addTo(map);
etape12
  .bindPopup(
    `<h2 id="popuptitle">12. Daria</h2>
  <p id="popup">Une jeune fille, avec un foulard autour du cou, débarque l'air assuré.  Peut-être a-t-elle une solution !</p>
  <audio controls src="https://cdn.exploreve.fr/content/voix/daria_meditation.mp3" id="media">Audio non supporté</audio>
  <p id="popup"><a href="https://cdn.exploreve.fr/content/transcriptions/meditation_daria.pdf" target="_blank" id="transcription">Transcription pour personnes sourdes et malentendantes</a></p>`
  )
  .on("click", marker12_click);

var etape13 = L.marker([46.908, -0.87299], { icon: pancarteIcon }).addTo(map);
etape13
  .bindPopup(
    `<h2 id="popuptitle">12. Le panneau bavard</h2>
  <p id="popup">Tu vois un panneau, comme sur les sentiers de randonnée. Tu t’approches pour lire&nbsp;:</p>
  <blockquote id="citation">Commença ici l'histoire peu Ordinaire d'une vache qu'on Nommait noémie. </br>
  elle voulait Tout connaître, tout savoir. Il lui semblait Ne pouvoir jamais réaliser Un si bel idéal. </br>
  Elle regardait les nuages, Rêvait qu'ils soient un Nouveau langage, une science Obscure qu'elle pourrait découvrir Rien qu'en se laissant Dériver mentalement, le coeur Tranquille. elle pouvait parfois Oublier de manger, si Une amie bienveillante ne Rouspétait pas un peu. </br></br>
  Noémie était donc frèle Et légère, elle ne Ressemblait pas aux autres. Guillerette fut-elle quand Apparut, un jour, derrière Un nuage, un ballon-songe. C'était si beau, si Haut dans le ciel. Elle était si heureuse, Son esprit dériva encore Un peu plus loin, Il s'envolait dans une Véritable nouvelle dimension. c'était Renversant d'être une pionnière, Exploratrice de l'inconnu. dès Son arrivée, tout en Elle lui criait de Ne plus jamais repartir. Tout à sa joie, Il lui apparut soudain Evident qu'elle aurait besoin Rapidement d'un ballon-songe.</br></br>
  Déterminée, elle se mit Immédiatement à concevoir cette Révolutionnaire machine. avec excitation, Elle conçut des plans Capables d'aider l'exploration de Tous·tes celles et ceux Irresistiblement attiré·es par ces Oniriques nouveaux paysages. quand Noémie eut terminé, elle Signa et cacha son Unique exemplaire, sourit puis Décolla, et ne revint jamais. c'est en son honneur qu'on nomma cet endroit la normande.</blockquote>`
  )
  .on("click", marker13_click);

var etape14 = L.marker([46.90542, -0.87944], { icon: afficheIcon }).addTo(map);
etape14
  .bindPopup(
    `<h2 id="popuptitle">13. Le labyrinthe</h2>
  <p id="popup">Tu arrives devant ce qui ressemble à des indications claires, et découvres qu’en fait, il n'en est rien...</p>
  <p id="popup"><a href="https://cdn.exploreve.fr/content/carte/labyrinthe.pdf" target="_blank">Afficher le labyrinthe en grand</a></p>`
  )
  .on("click", marker_click);

L.marker([46.91313, -0.87809], { icon: leonIcon }).addTo(map).on("click", leon_click);

function marker_click(e) {
  last_pos = e.target.getLatLng();
  last_pos = L.latLng(last_pos.lat + 0.0001, last_pos.lng);
  map.setView(last_pos);
}

function marker3_click(e) {
  last_pos = e.target.getLatLng();
  last_pos = L.latLng(last_pos.lat + 0.00025, last_pos.lng);
  map.setView(last_pos);
  music2.pause();
  music2.currentTime = 0;
}

function marker4_click(e) {
  last_pos = e.target.getLatLng();
  last_pos = L.latLng(last_pos.lat + 0.00025, last_pos.lng);
  map.setView(last_pos);
  music2.play().catch(handlePlayError);
}

function marker5_click(e) {
  last_pos = e.target.getLatLng();
  last_pos = L.latLng(last_pos.lat + 0.00025, last_pos.lng);
  music2.pause();
  music2.currentTime = 0;
  music3.play().catch(handlePlayError);
}

function marker6_click(e) {
  last_pos = e.target.getLatLng();
  last_pos = L.latLng(last_pos.lat + 0.00035, last_pos.lng);
  map.setView(last_pos);
}

function marker7_click(e) {
  last_pos = e.target.getLatLng();
  last_pos = L.latLng(last_pos.lat + 0.00025, last_pos.lng);
  music3.pause();
  music3.currentTime = 9;
  music2.play().catch(handlePlayError);
}

function marker12_click(e) {
  marker_click(e);
  music2.pause();
  music2.currentTime = 0;
}

function marker13_click(e) {
  marker_click(e);
  music2.play().catch(handlePlayError);
}

async function leon_click() {
  /*
   * This is a dirty trick because we don't have time.
   */
  localStorage.setItem("level_1", "6");
  window.location.href = "/immersion01/6";
}

/* eslint-disable no-unused-vars */
function go_back() {
  map.setView(last_pos);
}
