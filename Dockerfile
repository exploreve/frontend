# Copyright (C) 2020 ExploRêve
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Full source code is available at <https://gitlab.com/exploreve/frontend>.

FROM node:14 AS builder
RUN mkdir /exploreve-frontend && apt-get update -yq
COPY package.json package-lock.json /exploreve-frontend/
WORKDIR /exploreve-frontend
RUN npm install
COPY ./ .
ARG VUE_APP_ENV_PRODUCTION=true
ENV VUE_APP_ENV_PRODUCTION=$VUE_APP_ENV_PRODUCTION
ARG VUE_APP_API_URL=https://api.exploreve.fr
ENV VUE_APP_API_URL=$VUE_APP_API_URL
ARG VUE_APP_API_TOKEN=gRW5ty4jw4thjr6h4etthr6768adt688hethgd651d68aetgj7gdg22
ENV VUE_APP_API_TOKEN=$VUE_APP_API_TOKEN
RUN npm run build:prod

FROM nginx:1.17.9-alpine AS runtime
COPY --from=builder /exploreve-frontend/dist /var/www
COPY ./default.conf /etc/nginx/conf.d/
RUN rm -rf /app
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
