# Tropofrontend

Bienvenue dans le frontend du site www.exploreve.fr.

Ce projet utilise le framework Vue.js. Il contient toute la logique front
et l'affichage dans le navigateur du site (blog, à propos…) et des immersions.

## Structure du projet

Les immersions sont les différentes étapes du jeu. Elles sont nommées immersion01,
immersion02, etc.

Tout le code intéressant est dans src/components.

Chaque immersion a ses propres composants Vue.js, dans les dossiers du nom de l'immersion.

Si une immersion se fait en plusieurs pages/étapes: chaque étape est un composant, nommé IM_X_Y.vue.
X = numéro de l'immersion, Y = numéro de la page.

Le reste des composants est situé en vrac dans src/components.

## Configurer le projet

Tu auras besoin de Node.js et npm.

### Récupérer les dépendances
```
npm install
```

### Lancer le serveur en mode développement
```
npm run serve
```

### Lancer les tests
```
npm run test
```

### Lancer le linter
```
npm run lint
# ou
npm run lint --fix # modifiera automatiquement les fichiers.
```

### Lancer le projet avec docker
Récupérer l'environnement

```
docker build -t exploreve-frontend .
docker run -d --name le-frontend -p 8080:80 exploreve-frontend
```

## Licence

Le projet est sous licence AGPLv3 dont le texte est disponible dans le fichier LICENSE.md

