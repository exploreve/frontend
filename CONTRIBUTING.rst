Comment contribuer
==================

Communauté
----------

Toute la communication Extérieure (aide pour les joueurs, informations sur le projet) se fait par:
 - `le blog`_;
 - la liste de diffusion (inscription par le blog);
 - la `page facebook`_ Explorêve;

.. _le blog: https://www.exploreve.fr/blog
.. _page facebook: https://www.facebook.com/ExploreveScout

Pour participer à la création/au développement d'explorêve, envoie un email à contact@exploreve.fr.

Un problème à remonter?
-----------------------

Envoie un email à contact@exploreve.fr ou crée une issue sur `le gitlab`_.

.. _le gitlab: https://gitlab.com/exploreve/frontend


Dans les deux cas, suis les recommandations suivantes:

- Explique ce que tu observes.
- Explique ce que tu aurais dû observer.
- Explique comment reproduire le problème.
- S'il y a un message d'erreur, copie le.
- Si l'affichage est compliqué à décrire, fais une impression d'écran.

Proposer des changements
------------------------

Tout se passe sur `le gitlab`_.

- Ce qui tourne sur www.exploreve.fr est dans la branche **master**. Ne doit y être que ce
  qui attrait à ce qui tourne en production.
- Ce qui tourne sur staging.exploreve.fr est dans la branche **develop**. Y est ce le
  travail en cours sur l'immersion suivante/la version suivante.

Pour contribuer au code, crée-toi une branche, mets y tes modifications puis crée une
`merge request`_.

.. _merge request: https://gitlab.com/exploreve/frontend/merge_requests/new

- Si tu résouds un bug de production crée la depuis master **à jour**::

        git checkout master
        git pull
        git checkout -b ma-branche-de-travail

- Si tu implémente une nouvelle fonctionnalité pour la prochaine immersion, crée la
   depuis develop **à jour**::

        git checkout develop
        git pull
        git checkout -b ma-branche-de-travail

Nomme ta branche en fonction du sujet traité. Si tu travailles sur une issue, appelle-la ``issue-XX-titre-de-l-issue``.

Pense à commit tes changements et à pousser ta branche sur le gitlab régulièrement::

        git add -p
        git commit
        git push -u origin ma-branche-de-travail # La première fois
        git push # les autres fois

Quand ta fonctionnalité est prête, crée une `merge request`_ vers la branche principale
(develop ou master).

Quand tu ajoutes un nouveau fichier, pense à mettre l'en-tête de licence. Des exemples pour JS, Vue,
HTML et CSS sont disponibles dans license_header.txt.

Outils et installation
----------------------

Lis le README.md

Mettre à jour la carte
----------------------

Le fichier js de la carte est dans ``public/mapscript.js``

les liens des assets de la page sont dans le fichier ``src/views/Testcarte.vue``

Licence
--------------------

Tout le code source publié est sous licence AGPLv3 dont le texte est disponible dans le fichier
LICENSE.md.
