/**
 * Copyright (C) 2020 ExploRêve
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Full source code is available at <https://gitlab.com/exploreve/frontend>.
 */

import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    game_state: "PAUSE",
    current_stage: 0,
    current_direction: 1,
    move: "",
    is_end: false,
    message: {}
  },
  mutations: {
    PLAY_STATE(state) {
      state.game_state = "PLAY";
    },
    PAUSE_STATE(state) {
      state.game_state = "PAUSE";
    },
    FINISHED_STATE(state) {
      state.current_stage++;
      state.game_state = "FINISHED";
    },
    FINISHED_LAST_STATE(state) {
      state.is_end = true;
      state.game_state = "FINISHED";
    },
    LECTURE_STATE(state) {
      state.game_state = "LECTURE";
    },
    MESSAGE_READ(state) {
      state.game_state = "MESSAGE_READ";
    },
    MESSAGE_WRITE(state) {
      state.game_state = "MESSAGE_WRITE";
    },
    CURRENT_DIRECTION(state, direction) {
      state.current_direction = direction;
    },
    MOVE(state, move) {
      state.move = move;
    },
    MESSAGE_RECEIVED(state, message) {
      state.message = message;
    }
  },
  actions: {
    changeState({ commit }, event) {
      switch (event) {
        case "PLAY":
          commit("PLAY_STATE");
          break;
        case "PAUSE":
          commit("PAUSE_STATE");
          break;
        case "FINISHED":
          commit("FINISHED_STATE");
          break;
        case "FINISHED_LAST":
          commit("FINISHED_LAST_STATE");
          break;
        case "LECTURE":
          commit("LECTURE_STATE");
          break;
        case "MESSAGE_READ":
          commit("MESSAGE_READ");
          break;
        case "MESSAGE_WRITE":
          commit("MESSAGE_WRITE");
          break;
        default:
          commit("PAUSE_STATE");
          break;
      }
    },

    changeCurrentDirection({ commit }, direction) {
      commit("CURRENT_DIRECTION", direction);
    },
    changeMoveDirection({ commit }, direction) {
      commit("MOVE", direction);
    },
    messageReceived({ commit }, message) {
      commit("MESSAGE_RECEIVED", message);
    }
  },
  getters: {
    game_state: (state) => {
      return state.game_state;
    },
    current_stage: (state) => {
      return state.current_stage;
    },
    is_end: (state) => {
      return state.is_end;
    },
    current_direction: (state) => {
      return state.current_direction;
    },
    move: (state) => {
      return state.move;
    },
    message: (state) => {
      return state.message;
    }
  }
});
