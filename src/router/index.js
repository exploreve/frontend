/**
 * Copyright (C) 2020 ExploRêve
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Full source code is available at <https://gitlab.com/exploreve/frontend>.
 */

import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Interceptor from "../services/ProgressTracker";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/sac",
    name: "Sac",
    component: function () {
      return import("../views/Sac.vue");
    }
  },
  {
    path: "/attente",
    name: "Page d'attente",
    component: function () {
      return import("@/views/immersions/attente.vue");
    }
  },
  {
    path: "/blog",
    name: "Blog",
    component: function () {
      return import("../views/Blog.vue");
    }
  },
  {
    path: "/blog/:id/:slug",
    name: "BlogShow",
    component: function () {
      return import("../views/BlogShow.vue");
    }
  },
  {
    path: "/a-propos",
    name: "APropos",
    component: function () {
      return import("../views/Apropos.vue");
    }
  },
  {
    path: "/scarf",
    name: "Scarf",
    component: function () {
      return import("../views/Scarf.vue");
    }
  },
  {
    path: "/immersion01",
    component: function () {
      return import("@/views/immersions/immersion01.vue");
    },
    children: [
      {
        path: "/", //intro
        component: function () {
          return import("@/components/immersion01/IM_1_0.vue");
        }
      },
      {
        path: "1", //dessin foulard
        component: function () {
          return import("@/components/immersion01/IM_1_1.vue");
        }
      },
      {
        path: "2", //affichage campement
        component: function () {
          return import("@/components/immersion01/IM_1_2.vue");
        }
      },
      {
        path: "3", //énigmes
        component: function () {
          return import("@/components/immersion01/IM_1_3.vue");
        }
      },
      {
        path: "4", //intro carte
        component: function () {
          return import("@/components/immersion01/IM_1_4.vue");
        }
      },
      {
        path: "5", // carte
        component: function () {
          return import("@/components/immersion01/IM_1_5.vue");
        }
      },
      {
        path: "6", //intro camp
        component: function () {
          return import("@/components/immersion01/IM_1_6.vue");
        }
      },
      {
        path: "7", //dessin camp
        component: function () {
          return import("@/components/immersion01/IM_1_7.vue");
        }
      },
      {
        path: "8", //affichage campement
        component: function () {
          return import("@/components/immersion01/IM_1_8.vue");
        }
      },
      {
        path: "9", //conclusion
        component: function () {
          return import("@/components/immersion01/IM_1_9.vue");
        }
      }
    ]
  },
  {
    path: "/immersion02",
    component: function () {
      return import("@/views/immersions/immersion02.vue");
    },
    children: [
      {
        path: "/", //intro
        component: function () {
          return import("@/components/immersion02/IM_2_0.vue");
        }
      },
      {
        path: "1", //transition
        component: function () {
          return import("@/components/immersion02/IM_2_0_1.vue");
        }
      },
      {
        path: "2", //cluedo
        component: function () {
          return import("@/components/immersion02/IM_2_1.vue");
        }
      },
      {
        path: "3", //transition
        component: function () {
          return import("@/components/immersion02/IM_2_2.vue");
        }
      },
      {
        path: "4", //placement souvenir
        component: function () {
          return import("@/components/immersion02/IM_2_3.vue");
        }
      },
      {
        path: "5", //affichage souvenirs
        component: function () {
          return import("@/components/immersion02/IM_2_4.vue");
        }
      },
      {
        path: "6", //wordcloud
        component: function () {
          return import("@/components/immersion02/IM_2_5.vue");
        }
      },
      {
        path: "7", //conclusion_mdp
        component: function () {
          return import("@/components/immersion02/IM_2_6.vue");
        }
      }
    ]
  },
  {
    path: "/immersion03",
    component: function () {
      return import("@/views/immersions/immersion03.vue");
    },
    children: [
      {
        path: "1", //intro
        component: function () {
          return import("@/components/immersion03/IM_3_1.vue");
        }
      },
      {
        path: "2", //règles
        component: function () {
          return import("@/components/immersion03/IM_3_2.vue");
        }
      },
      {
        path: "3", //jeu
        component: function () {
          return import("@/components/immersion03/IM_3_3.vue");
        }
      },
      {
        path: "4", //conclusion
        component: function () {
          return import("@/components/immersion03/IM_3_4.vue");
        }
      }
    ]
  },
  {
    path: "/immersion04",
    component: function () {
      return import("@/views/immersions/immersion04.vue");
    },
    children: [
      {
        path: "1", //intro
        component: function () {
          return import("@/components/immersion04/IM_4_1.vue");
        }
      },
      {
        path: "2", //règles
        component: function () {
          return import("@/components/immersion04/IM_4_2.vue");
        }
      },
      {
        path: "3", //jeu
        component: function () {
          return import("@/components/immersion04/IM_4_3.vue");
        }
      }
    ]
  },
  {
    path: "*",
    name: "404",
    component: function () {
      return import("@/views/404.vue");
    }
  }
];
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(Interceptor.tracker);

export default router;
