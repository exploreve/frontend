/**
 * Copyright (C) 2020 ExploRêve
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Full source code is available at <https://gitlab.com/exploreve/frontend>.
 */

import L from "leaflet";
import API from "./ApiService";
export default {
  getIcon: function () {
    return L.icon({
      iconUrl: "https://cdn.exploreve.fr/content/drawing/IconeCroix.svg",
      iconSize: [24, 24]
    });
  },
  submitMemory: async function (user) {
    try {
      return API.post("/souvenirs", user);
    } catch (error) {
      throw new Error(error);
    }
  },
  getAll: async function () {
    const request = await API.get("/souvenirs");
    let memories = request.data.souvenirs;
    memories.forEach(async (memory) => {
      memory.coords = [memory.lat, memory.lng];
    });
    return memories;
  },
  submitScarf: async function (scarf) {
    try {
      const request = await API.post(`/scarfs/`, scarf);
      if (!request.data.success) throw new Error(request.data);
      return request.data.scarf;
    } catch (error) {
      throw new Error(error);
    }
  },
  getWordCloud: async function () {
    const request = await API.get(`/souvenirs/wordcloud`);
    const words = request.data.word_cloud.map((r) => {
      return [r.word, r.weight];
    });
    return words;
  }
};
