/**
 * Copyright (C) 2020 ExploRêve
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Full source code is available at <https://gitlab.com/exploreve/frontend>.
 */

import * as Sentry from "@sentry/browser";
import ApiService from "@/services/ApiService";

const API_URL = "/progress/trackers/";
let immersion_id = 1;
let last_level_done = localStorage.getItem(`level_${immersion_id}`);
const regex = new RegExp(/\/immersion\d{2}\/?/);

async function $initTracker(immersion) {
  if (localStorage.getItem(`id_${immersion}`)) return Promise.resolve(0);
  const response = await ApiService.post(API_URL, { immersion_id: immersion });
  last_level_done = response.data.new_tracker.last_level_done;
  localStorage.setItem(`level_${immersion}`, last_level_done);
  localStorage.setItem(`id_${immersion}`, response.data.new_tracker.id);
  return Promise.resolve(0);
}

export default {
  tracker(to, from, next) {
    if (process.env.VUE_APP_ENV_PRODUCTION === "false") return next();
    if (!regex.test(to.path)) return next();
    immersion_id = Number(to.path.replace("/immersion0", "").replace(/\/.*/, ""));
    $initTracker(immersion_id)
      .then(() => {
        let current_level = Number(from.path.replace(regex, ``).replace("/", ""));
        let target_level = Number(to.path.replace(regex, ``).replace("/", ""));
        if (target_level === 0) return next();
        if (current_level > target_level) return next();
        last_level_done = Number(localStorage.getItem(`level_${immersion_id}`));
        // Allow all levels when immersion01 is finished.
        if ("yes" !== localStorage.getItem(`immersion0${immersion_id}_finished`)) {
          if (target_level < last_level_done) {
            return next(`/immersion0${immersion_id}/${last_level_done}`);
          }
          if (target_level > last_level_done) {
            return next(`/immersion0${immersion_id}/${last_level_done}`);
          }
        }
        return next();
      })
      .catch((err) => {
        Sentry.configureScope(function (user) {
          user.setUser({ id: localStorage.getItem(`id_${immersion_id}`) });
          user.setTag("immersion", immersion_id);
          user.setTag("last level", last_level_done);
          user.setTag("phase", "Progress tracker issue");
          user.setLevel("fatal");
        });
        Sentry.captureException(new Error(err));
        return next();
      });
  },
  resetTracker(imm_id) {
    window.localStorage.removeItem(`id_${imm_id ?? immersion_id}`);
    window.localStorage.removeItem(`level_${imm_id ?? immersion_id}`);
  },
  resetTrackerImmersion(imm_id) {
    this.resetTracker(imm_id);
    window.localStorage.setItem(`immersion0${imm_id ?? immersion_id}_finished`, "yes");
  },
  async updateTracker() {
    let id = Number(localStorage.getItem(`id_${immersion_id}`));
    let current_level = Number(localStorage.getItem(`level_${immersion_id}`));
    immersion_id = Number(window.location.pathname.replace("/immersion0", "").replace(/\/.*/, ""));
    if (current_level !== 9) current_level++;
    if (isNaN(id) || id === 0 || isNaN(current_level) || current_level === 0) return;
    return ApiService.patch(API_URL, { id, last_level_done: current_level })
      .then((e) => {
        if (e.data.success) {
          localStorage.setItem(`level_${immersion_id}`, e.data.updated_tracker.last_level_done);
          return Promise.resolve(0);
        }
      })
      .catch((err) => {
        Sentry.configureScope(function (user) {
          user.setUser({ id: localStorage.getItem(`id_${immersion_id}`) });
          user.setTag("immersion", immersion_id);
          user.setTag("last level", last_level_done);
          user.setTag("phase", "Progress tracker issue");
          user.setLevel("fatal");
        });
        Sentry.captureException(err);
      });
  },
  async forceImmersionId(immersion) {
    return await $initTracker(immersion);
  }
};
