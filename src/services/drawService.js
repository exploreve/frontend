/**
 * Copyright (C) 2020 ExploRêve
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Full source code is available at <https://gitlab.com/exploreve/frontend>.
 */

import Axios from "axios";
import * as Sentry from "@sentry/browser";
import ApiService from "@/services/ApiService";

var pixels = [];

export default {
  askForTile: async function () {
    const tile_data = await ApiService.post("/camp/tiles");
    var tile = tile_data.data.tile;
    sessionStorage.setItem("tilesId", tile.id);
    sessionStorage.setItem("tilesX", tile.x);
    sessionStorage.setItem("tilesY", tile.y);
    return tile;
  },
  getIdsForFinal: async function (p) {
    const tile = await this.askForTile();
    var x = tile.x;
    var y = tile.y;
    pixels = p;
    let response;
    try {
      response = await ApiService.get(`/camp/tiles/${x}/${y}/form`);
      return response.data.tile;
    } catch (e) {
      Sentry.configureScope(function (user) {
        user.setUser({ id: sessionStorage.getItem("tilesId") });
        user.setTag("tilesX", tile.x);
        user.setTag("tilesY", tile.y);
        user.setTag("phase", "Get upload creds to cellar (from API)");
        user.setLevel("fatal");
      });
      Sentry.captureException(new Error(e));
    }
  },
  uploadFinal: async function (data) {
    if (!data) return;
    let form = data.form;
    let url = form.url;
    let image = await new Promise((resolve) => {
      var canvas = document.createElement("canvas");
      canvas.width = 540;
      canvas.height = 540;
      var ctx = canvas.getContext("2d");
      for (var i = 0; i < pixels.length; i++) {
        var s = 12;
        var x = i % 45;
        var y = Math.floor(i / 45);
        ctx.fillStyle = pixels[i];
        ctx.fillRect(x * s, y * s, s, s);
      }
      canvas.toBlob(function (blob) {
        resolve(blob);
      });
    });
    const formData = new FormData();
    formData.append("key", form.fields.key);
    formData.append("AWSAccessKeyId", form.fields.AWSAccessKeyId);
    formData.append("policy", form.fields.policy);
    formData.append("signature", form.fields.signature);
    formData.append("file", image);
    return await Axios.post(url, formData);
  },
  getAllTiles: async function () {
    const tiles = (await ApiService.get(`/camp/tiles/safe/url`)).data.tiles;
    const maxes = tiles.reduce(
      (maxes, t) => {
        return {
          max_id: Math.max(maxes.max_id, t.id),
          min_x: Math.min(maxes.min_x, t.x),
          min_y: Math.min(maxes.min_y, t.y),
          max_x: Math.max(maxes.max_x, t.x),
          max_y: Math.max(maxes.max_y, t.y)
        };
      },
      { min_x: 0, max_x: 0, min_y: 0, max_y: 0, max_id: 0 }
    );

    const diff = maxes.max_x - maxes.min_x;
    const minmin = 16;
    if (diff < minmin) {
      maxes.min_x += -(minmin / 2) - maxes.min_x + 1;
      maxes.max_x += minmin / 2 - maxes.max_x;
      maxes.min_y += -(minmin / 2) - maxes.min_y + 2;
      maxes.max_y += minmin / 2 - maxes.max_y;
    } else {
      maxes.min_x -= 2;
      maxes.max_x += 2;
      maxes.min_y -= 2;
      maxes.max_y += 2;
    }

    let mid = maxes.max_id;

    for (let x = maxes.min_x; x <= maxes.max_x; ++x) {
      for (let y = maxes.min_y; y <= maxes.max_y; ++y) {
        if (typeof tiles.find((el) => el.x == x && el.y == y) === "undefined") {
          tiles.push({
            x,
            y,
            img_url: "https://cdn.exploreve.fr/content/drawing/notfound.png",
            fake: true,
            id: ++mid
          });
        }
      }
    }

    const sorted = tiles.sort((t1, t2) => {
      if (t1.y - t2.y == 0) {
        return t1.x - t2.x;
      } else {
        return t1.y - t2.y;
      }
    });

    return {
      images: sorted,
      style_attr: "--cols: " + (maxes.max_x - maxes.min_x + 1) + "; --rows: " + (maxes.max_y - maxes.min_y + 1) + ";"
    };
  }
};
