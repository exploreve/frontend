/**
 * Copyright (C) 2020 ExploRêve
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Full source code is available at <https://gitlab.com/exploreve/frontend>.
 */

import axios from "axios";

const API_URL = process.env.VUE_APP_API_URL;
const API_TOKEN = process.env.VUE_APP_API_TOKEN;

export default {
  post(relativeUrl, body) {
    return axios({
      method: "post",
      url: API_URL + relativeUrl,
      data: body,
      validateStatus: false,
      headers: {
        "exploreve-api-auth-token": API_TOKEN
      }
    });
  },
  get(relativeUrl) {
    return axios({
      method: "get",
      url: API_URL + relativeUrl,
      headers: {
        "exploreve-api-auth-token": API_TOKEN
      }
    });
  },
  patch(relativeUrl, data) {
    return axios({
      method: "patch",
      url: API_URL + relativeUrl,
      data: data,
      headers: {
        "exploreve-api-auth-token": API_TOKEN
      }
    });
  },
  postFormData(relativeUrl, body) {
    return axios({
      method: "post",
      url: API_URL + relativeUrl,
      data: body,
      headers: {
        "content-type": `multipart/form-data`,
        "exploreve-api-auth-token": API_TOKEN
      }
    });
  }
};
