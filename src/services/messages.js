/**
 * Copyright (C) 2020 ExploRêve
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Full source code is available at <https://gitlab.com/exploreve/frontend>.
 */

import API from "./ApiService";
export default {
  submit: async function (message) {
    const response = await API.post("/messages", message);
    if (response.data && response.data.message && response.data.message.id) {
      localStorage.setItem("user_message_id", response.data.message.id);
    }
    return response.data;
  },
  getRandomMessage: async function () {
    const request = await API.get(`/messages/random/${localStorage.getItem("user_message_id") || 0}`);
    let memories = request.data.message;
    return memories;
  }
};
