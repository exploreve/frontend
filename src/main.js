/**
 * Copyright (C) 2020 ExploRêve
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Full source code is available at <https://gitlab.com/exploreve/frontend>.
 */

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import {
  BModal,
  BButton,
  BFormInput,
  BIconEnvelope,
  VBTooltip,
  BTooltip,
  ToastPlugin,
  BIconVolumeUp,
  BIconVolumeMute,
  BFormSelect,
  BFormGroup,
  BFormInvalidFeedback,
  BFormTextarea,
  BCard,
  BIconXCircle,
  BIconSquare,
  BIconCheckBox,
  BContainer,
  BRow,
  BCol
} from "bootstrap-vue";
import * as Sentry from "@sentry/browser";
import * as Integrations from "@sentry/integrations";
import VueKonva from "vue-konva";

Sentry.init({
  dsn: "https://c34e24e6a5a9462580275d4ca929d455@sentry.exploreve.fr/1",
  integrations: [new Integrations.Vue({ Vue, attachProps: true, logErrors: true })],
  beforeSend(event) {
    // Check if it is an exception, and if so, show the report dialog
    if (event.exception) {
      if (!/AbortError/.test(event.exception.values[0].value)) {
        Sentry.showReportDialog({
          eventId: event.event_id,
          title: "Oh non, une erreur est survenue...",
          subtitle:
            "N'hésitez pas à nous faire part de l'erreur rencontrée pour que nous la corrigions le plus vite possible.",
          labelName: "Nom",
          labelEmail: "Email",
          labelComments: "Que s'est-il passé ?",
          labelClose: "Fermer",
          labelSubmit: "Envoyer !"
        });
      }
    }
    return event;
  }
});

import "./assets/styles/bootstrap-vue.scss";
import "leaflet/dist/leaflet.css";

// Bootstrap Vue
// Bootstrap Vue
Vue.component("b-form-select", BFormSelect);
Vue.component("b-container", BContainer);
Vue.component("b-row", BRow);
Vue.component("b-col", BCol);

Vue.component("b-button", BButton);
Vue.component("b-modal", BModal);
Vue.component("b-form-group", BFormGroup);
Vue.component("b-form-invalid-feedback", BFormInvalidFeedback);
Vue.component("b-form-input", BFormInput);
Vue.component("BIconEnvelope", BIconEnvelope);
Vue.component("b-icon-volume-up", BIconVolumeUp);
Vue.component("b-icon-volume-mute", BIconVolumeMute);
Vue.component("b-icon-x-circle", BIconXCircle);
Vue.component("b-icon-square", BIconSquare);
Vue.component("b-icon-check-box", BIconCheckBox);
Vue.component("BTooltip", BTooltip);
Vue.component("BCard", BCard);
Vue.directive("b-tooltip", VBTooltip);
Vue.component("b-form-textarea", BFormTextarea);
Vue.use(ToastPlugin);
Vue.use(VueKonva);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: function (h) {
    return h(App);
  }
}).$mount("#app");
