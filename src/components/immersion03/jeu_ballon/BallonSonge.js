import Vector from "victor";

export default class BallonSonge {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.speed = 1;
    this.checkpoint_threshold = 8;
  }
  getVector() {
    return new Vector(this.x, this.y);
  }
  getVectorFrom(vector) {
    return this.getVector().subtract(vector);
  }
  getDistance(array) {
    return Math.sqrt(Math.pow(array[0] - this.x, 2) + Math.pow(array[1] - this.y, 2));
  }
  compareTo(pos1, pos2) {
    const pos1_pos2_vector = Vector.fromArray(pos1).subtract(Vector.fromArray(pos2));
    if (pos1_pos2_vector.y === 0) {
      return this.getDistance(pos2) <= this.checkpoint_threshold && Math.abs(this.x - pos2[0]) <= 5;
    }
    if (pos1_pos2_vector.x === 0) {
      return this.getDistance(pos2) <= this.checkpoint_threshold && Math.abs(this.y - pos2[1]) <= 5;
    }
    return false;
  }
  isOutOfTrajectory(pos1, pos2) {
    const pos1_pos2_vector = Vector.fromArray(pos1).subtract(Vector.fromArray(pos2));
    if (pos1_pos2_vector.y === 0) {
      return Math.abs(this.y - pos1[1]) > this.checkpoint_threshold && !this.compareTo(pos1, pos2);
    }
    if (pos1_pos2_vector.x === 0) {
      return Math.abs(this.x - pos1[0]) > this.checkpoint_threshold && !this.compareTo(pos1, pos2);
    }
    return false;
  }
  projectBallonInTrajectory(pos1, pos2) {
    const pos1_pos2_vector = Vector.fromArray(pos1).subtract(Vector.fromArray(pos2));
    if (pos1_pos2_vector.x === 0) {
      this.x = pos1[0];
    } else if (pos1_pos2_vector.y === 0) {
      this.y = pos1[1];
    }
  }
  moveUp() {
    if (this.y === 0) {
      return;
    }
    this.y -= this.speed;
  }
  moveLeft() {
    if (this.x === 0) {
      return;
    }
    this.x -= this.speed;
  }
  moveDown() {
    if (this.y === 350) {
      return;
    }
    this.y += this.speed;
  }
  moveRight() {
    if (this.x === 350) {
      return;
    }
    this.x += this.speed;
  }
  setPos(x, y) {
    this.x = x;
    this.y = y;
  }
}
